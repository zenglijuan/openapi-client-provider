package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankResponse;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankNotifyResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/25 14:01
 */
@Getter
@Setter
@XStreamAlias("document")
public class WsbankNotifyResponse extends WsbankResponse {

    /**
     * 响应报文信息
     */
    @XStreamAlias("response")
    private WsbankNotifyResponseInfo responseInfo;
}
