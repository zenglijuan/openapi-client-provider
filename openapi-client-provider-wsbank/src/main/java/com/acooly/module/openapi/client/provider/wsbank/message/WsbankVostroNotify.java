package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankNotify;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankVostroNotifyInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/24 14:47
 */
@Getter
@Setter
@XStreamAlias("document")
@WsbankApiMsgInfo(service = WsbankServiceEnum.VOSTRO_NOTIFY,type = ApiMessageType.Notify)
public class WsbankVostroNotify extends WsbankNotify {

    /**
     * 来帐汇入通知
     */
    @XStreamAlias("request")
    private WsbankVostroNotifyInfo requestInfo;
}
