package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankResponse;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankWithdrawApplyResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XStreamAlias("document")
@WsbankApiMsgInfo(service = WsbankServiceEnum.WITHDRAW_APPLY,type = ApiMessageType.Response)
public class WsbankWithdrawApplyResponse extends WsbankResponse {

    @XStreamAlias("response")
    private WsbankWithdrawApplyResponseInfo responseInfo;
}
