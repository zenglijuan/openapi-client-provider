package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsbankOpenPayResponseBody implements Serializable {

    /**
     * 返回码组件。当ResultStatus=S时才有后续的参数返回。
     */
    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;

    /**
     * 外部交易号
     */
    @XStreamAlias("OutTradeNo")
    private String outTradeNo;
    
    /**
     * 外部交易号
     */
    @XStreamAlias("MerchantId")
    private String merchantId;
    
    
    /**
     * 外部交易号
     */
    @XStreamAlias("IsvOrgId")
    private String isvOrgId;
}
