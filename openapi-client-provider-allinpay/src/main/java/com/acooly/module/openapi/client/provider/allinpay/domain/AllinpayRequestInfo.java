package com.acooly.module.openapi.client.provider.allinpay.domain;

import com.acooly.core.utils.Ids;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @Auther: zhike
 * @Date: 2018/8/29 11:54
 * @Description:
 */
@Getter
@Setter
@ToString
@XStreamAlias("INFO")
public class AllinpayRequestInfo implements Serializable {

    /**
     * 交易代码
     * 每种报文的交易代码都不一样，见具体的报文定义
     */
    @XStreamAlias("TRX_CODE")
    @NotBlank
    @Size(max = 20)
    private String trxCode;

    /**
     * 版本
     * 每种报文的版本都可能不一样，见具体的报文定义
     */
    @XStreamAlias("VERSION")
    @NotBlank
    @Size(max = 2)
    private String version;

    /**
     * 数据格式
     * 目前固定为2，XML方式
     */
    @XStreamAlias("DATA_TYPE")
    @NotBlank
    @Size(max = 1)
    private String dataType = "2";

    /**
     * 处理级别
     * 0-9  0优先级最低，目前取固定值5
     */
    @XStreamAlias("LEVEL")
    @NotBlank
    @Size(max = 1)
    private String level = "5";

    /**
     * 商户代码
     * 通联通分配的15位商户号。测试环境参数找通联客户经理要。
     */
    @XStreamAlias("MERCHANT_ID")
    @NotBlank
    @Size(max = 15)
    private String merchantId;

    /**
     * 用户名
     * 通联通分配的对接用户，一般为商户号+04。测试环境参数找通联客户经理要。
     */
    @XStreamAlias("USER_NAME")
    @NotBlank
    @Size(max = 20)
    private String userName;

    /**
     * 用户密码
     * 通联通分配的对接用户密码。测试环境参数找通联客户经理要。
     */
    @XStreamAlias("USER_PASS")
    @NotBlank
    @Size(max = 20)
    private String userPass;

    /**
     * 交易批次号
     * 需在通联通保证全局唯一，建议按商户号+时间戳（YYYYMMDDmmSS）+商户自定义的规则
     */
    @XStreamAlias("REQ_SN")
    @NotBlank
    @Size(max = 40)
    private String reqSn = Ids.oid();

    /**
     * 签名信息
     */
    @XStreamAlias("SIGNED_MSG")
    private String signedMsg = "sign";

}
