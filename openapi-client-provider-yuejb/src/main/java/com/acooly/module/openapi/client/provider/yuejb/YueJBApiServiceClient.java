package com.acooly.module.openapi.client.provider.yuejb;

import com.acooly.core.utils.net.HttpResult;
import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.yuejb.domain.YueJBNotify;
import com.acooly.module.openapi.client.provider.yuejb.domain.YueJBRequest;
import com.acooly.module.openapi.client.provider.yuejb.domain.YueJBResponse;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Created by ouwen@yiji.com} on 2017/11/6.
 */
@Component
public class YueJBApiServiceClient extends AbstractApiServiceClient<YueJBRequest, YueJBResponse, YueJBNotify, YueJBNotify> {

    private static Logger logger = LoggerFactory.getLogger(YueJBApiServiceClient.class);

    @Resource(name = "yuejbHttpTransport")
    private Transport transport;
    @Resource(name = "yueJBRequestMarshal")
    private ApiMarshal<String, YueJBRequest> requestMarshal;
    @Resource(name = "yueJBResponseUnmarshal")
    private ApiUnmarshal<YueJBResponse, HttpResult> responseUnmarshal;
    @Resource(name = "yueJBNotifyUnmarshal")
    private ApiUnmarshal<YueJBNotify, Map<String, String>> notifyUnmarshal;
    private ApiMarshal<String, YueJBRequest> redirectMarshal;

    @Autowired
    private OpenAPIClientYueJBProperties openAPIClientYueJBProperties;

    @Override
    public YueJBResponse execute(YueJBRequest request) {
        try {
            beforeExecute(request);
            String url = openAPIClientYueJBProperties.getGateway();
            String requestMessage = getRequestMarshal().marshal(request);
            /**
             Map<String,String> dataMap = Maps.newTreeMap();
            String[] keyValues = requestMessage.split("&");
            for(String kvStr :keyValues){
                String[] kv= kvStr.split("=");
                dataMap.put(kv[0],kv[1]);
            }
            HttpResult result = Https.getInstance().post(url,dataMap,"UTF-8");
             */
             /**以上方式等同*/
            HttpResult result = getTransport().request(requestMessage, url);

            YueJBResponse t = getResponseUnmarshalSpec().unmarshal(result, request.getService());
            afterExecute(t);
            return t;
        } catch (ApiServerException ose) {
            logger.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            logger.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            logger.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }
    @Override
    protected void beforeExecute(YueJBRequest request) {
        request.setNotifyUrl(openAPIClientYueJBProperties.getDomain()+openAPIClientYueJBProperties.getNotifyUrl());
        request.setVersion(openAPIClientYueJBProperties.getVersion());
        request.setSignType(openAPIClientYueJBProperties.getSignType());
        //月结宝分配商户号
        request.setPartner(openAPIClientYueJBProperties.getPartnerId());
        super.beforeExecute(request);
    }

    @Override
    protected ApiMarshal<String, YueJBRequest> getRequestMarshal() {
        return this.requestMarshal;
    }

    protected ApiUnmarshal<YueJBResponse, HttpResult> getResponseUnmarshalSpec() {
        return this.responseUnmarshal;
    }

    @Override
    protected ApiUnmarshal<YueJBResponse, String> getResponseUnmarshal() {
        return null;
    }

    @Override
    protected ApiMarshal<String, YueJBRequest> getRedirectMarshal() {
        return this.redirectMarshal;
    }

    @Override
    protected ApiUnmarshal<YueJBNotify, Map<String, String>> getNoticeUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected ApiUnmarshal<YueJBNotify, Map<String, String>> getReturnUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected Transport getTransport() {
        return this.transport;
    }

    @Override
    public String getName() {
        return YueJBConstants.PROVIDER_NAME;
    }
}
