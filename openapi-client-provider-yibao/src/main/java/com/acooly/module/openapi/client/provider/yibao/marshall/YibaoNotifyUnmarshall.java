/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.yibao.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.provider.yibao.YibaoConstants;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoNotify;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhangpu
 */
@Slf4j
@Component
public class YibaoNotifyUnmarshall extends YibaoAbstractMarshall implements ApiUnmarshal<YibaoNotify, Map<String, String>> {
    
    @Override
    public YibaoNotify unmarshal(Map<String, String> message, String serviceKey) {
        String crypt = message.get(YibaoConstants.REQUEST_PARAM_NAME);
        return (YibaoNotify) doUnMarshall(crypt, serviceKey, true);
    }
}
