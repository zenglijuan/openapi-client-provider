/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-16 16:11 创建
 */
package com.acooly.module.openapi.client.provider.fudian;

import com.acooly.core.utils.security.RSA;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.acooly.module.openapi.client.provider.fudian.OpenAPIClientFudianProperties.PREFIX;


/**
 * 富滇银行P2P存管配置参数
 *
 * @author zhangpu@acooly.cn
 */
@Getter
@Setter
@ConfigurationProperties(prefix = PREFIX)
public class OpenAPIClientFudianProperties {

    public static final String PREFIX = "acooly.openapi.client.fudian";

    /**
     * 网关地址
     */
    private String gatewayUrl;


    /**
     * 下载网关地址
     */
    private String downloadUrl;

    /**
     * 商户号
     */
    private String merchantNo;


    /**
     * 商户keystore(filepath)
     */
    private String keystore;

    /**
     * 商户keystore密码
     */
    private String keystorePswd;

    /**
     * 签名算法
    */
    private String signatureAlgo = RSA.SIGN_ALGO_SHA1;

    /**
     * 网关证书(filepath)
     */
    private String gatewayCert;


    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    /**
     * 本系统的通知Url或URL前缀
     */
    private String notifyUrl;

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;


    private Checkfile checkfile = new Checkfile();

    @Getter
    @Setter
    public static class Checkfile {

        private String host;
        private int port;
        private String username;
        private String password;

        private String serverRoot;
        private String localRoot;

    }


}
