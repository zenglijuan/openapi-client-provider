package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinaflagType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayAccountType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 * 3.11 充值查询
 *
 * @author xiaohong
 * @create 2018-07-18 9:54
 **/
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_HOSTING_DEPOSIT, type = ApiMessageType.Request)
public class QueryHostingDepositRequest extends SinapayRequest {
    /**
     * 用户标识信息
     */
    @NotEmpty
    @Size(max = 50)
    @ApiItem(value = "identity_id")
    private String identityId;

    /**
     * 用户标识类型
     *
     * ID的类型，参考“标志类型”
     */
    @NotEmpty
    @Size(max = 16)
    @ApiItem(value = "identity_type")
    private String identityType = SinaflagType.UID.getCode();

    /**
     * 账户类型
     *
     * 账户类型（基本户、保证金户）。默认基本户
     */
    @Size(max = 16)
    @ApiItem(value = "account_type")
    private String accountType = SinapayAccountType.BASIC.code();


    /**
     * 交易订单号
     *
     * 账户类型（基本户、保证金户）。默认基本户
     */
    @Size(max = 32)
    @ApiItem(value = "out_trade_no")
    private String outTradeNo;

    /**
     * 开始时间
     *
     * 数字串，一共14位 格式为：年[4位]月[2位]日[2位]时[2位]分[2位]秒[2位]
     */
    @Size(max = 14)
    @ApiItem(value = "start_time")
    private String startTime;

    /**
     * 结束时间
     *
     * 数字串，一共14位 格式为：年[4位]月[2位]日[2位]时[2位]分[2位]秒[2位]
     */
    @Size(max = 14)
    @ApiItem(value = "end_time")
    private String endTime;

    /**
     * 页号
     */
    @Min(1)
    @ApiItem(value = "page_no")
    private int pageNo = 1;

    /**
     * 每页大小
     */
    @ApiItem(value = "page_size")
    private int pageSize = 20;
}
