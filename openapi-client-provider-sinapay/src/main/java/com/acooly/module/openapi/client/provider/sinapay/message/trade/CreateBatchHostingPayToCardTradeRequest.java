package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinaNotifyMethod;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinaPaytoType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 3.15创建批量代付到提现卡交易
 *
 * @author xiaohong
 * @create 2018-07-18 14:56
 **/
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_BATCH_HOSTING_PAY_TO_CARD_TRADE, type = ApiMessageType.Request)
public class CreateBatchHostingPayToCardTradeRequest extends SinapayRequest {

    /**
     * 支付请求号
     *
     * 商户网站支付订单号，商户内部保证唯一
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "out_pay_no")
    private String outPayNo;

    /**
     * 交易码
     *
     * 商户网站代收交易业务码，见附录
     */
    @NotEmpty
    @Size(max = 16)
    @ApiItem(value = "out_trade_code")
    private String outTradeCode;

    /**
     * 交易列表
     *
     * 详见“交易参数”。参数间用“~”分隔，各条目之间用“$”分隔，备注信息不要包含特殊分隔符
     */
    @NotEmpty
    @ApiItem(value = "trade_list")
    private String tradeList;

    /**
     * 通知方式
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "notify_method")
    private String notifyMethod = SinaNotifyMethod.SINGLE_NOTIFY.getCode();

    /**
     * 到账类型
     *
     * GENERAL： 普通
     * FAST: 快速
     */
    @Size(max = 16)
    @ApiItem(value = "payto_type")
    private String paytoType = SinaPaytoType.GENERAL.getCode();

    /**
     * 用户IP地址
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "user_ip")
    private String userIp;
}
