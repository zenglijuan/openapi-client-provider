/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月6日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.api.exception.ApiMessageCheckException;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 * 
 * 交易订单查询，可用于查询补单，也可用于页面查询显示。
 * <li>每页记录数不超过20；
 * <li>交易号和时间至少一项存在，同时存在以交易号为准；
 * <li>开始时间和结束时间须同时存在。
 * <li>当交易订单号为空时，用户标志和标志类型不能为空。
 * 
 * 
 * @author zhike
 */
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_HOSTING_TRADE, type = ApiMessageType.Request)
public class QueryHostingTradeRequest extends SinapayRequest {

	/**
	 * 用户标识信息
	 *
	 * 商户系统用户ID(字母或数字)
	 */
	@Size(max = 50)
	@ApiItem(value = "identity_id")
	private String identityId;

	/**
	 * 用户标识类型
	 *
	 * ID的类型，参考“标志类型”
	 */
	@Size(max = 16)
	@ApiItem(value = "identity_type")
	private String identityType = "UID";

	/**
	 * 交易订单号
	 *
	 * 商户网站交易订单号，商户内部保证唯一
	 */
	@Size(max = 32)
	@ApiItem(value = "out_trade_no")
	private String outTradeNo;

	/**
	 * 开始时间
	 *
	 * 数字串，一共14位 格式为：年[4位]月[2位]日[2位]时[2位]分[2位]秒[2位]
	 */
	@Size(max = 14)
	@ApiItem(value = "start_time")
	private String startTime;

	/**
	 * 结束时间
	 *
	 * 数字串，一共14位 格式为：年[4位]月[2位]日[2位]时[2位]分[2位]秒[2位]
	 */
	@Size(max = 14)
	@ApiItem(value = "end_time")
	private String endTime;

	/**
	 * 页号
	 *
	 * 页号，从1开始，默认为1
	 */
	@Min(1)
	@ApiItem(value = "page_no")
	private int pageNo = 1;

	/**
	 * 每页大小
	 *
	 * 每页记录数，默认20
	 */
	@Min(1)
	@Max(1000)
	@ApiItem(value = "page_size")
	private int pageSize = 20;

	public QueryHostingTradeRequest() {
		super();
	}

	/**
	 * 根据订单号查询
	 * 
	 * @param outTradeNo
	 */
	public QueryHostingTradeRequest(String outTradeNo) {
		super();
		this.outTradeNo = outTradeNo;
	}

	/**
	 * 根据时间段查询
	 * 
	 * @param identityId
	 * @param startTime
	 * @param endTime
	 */
	public QueryHostingTradeRequest(String identityId, String startTime, String endTime) {
		super();
		this.identityId = identityId;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	@Override
	public void doCheck() throws ApiMessageCheckException {
		if (Strings.isBlank(outTradeNo)) {

		}
		super.doCheck();
	}

	public String getIdentityId() {
		return identityId;
	}

	public void setIdentityId(String identityId) {
		this.identityId = identityId;
	}

	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

}
