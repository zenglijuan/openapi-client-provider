package com.acooly.module.openapi.client.provider.bosc.domain;

import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.bosc.annotation.BoscAlias;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoscBaseDomain implements ApiMessage {

	/**
	 * 商户标识
	 */
	@BoscAlias(isSign = false)
	private String partnerId;

	@BoscAlias(isSign = false)
	private String service;

	public String getService() {
		return service;
	}

	@Override
	public String getPartner() {
		return partnerId;
	}

}
