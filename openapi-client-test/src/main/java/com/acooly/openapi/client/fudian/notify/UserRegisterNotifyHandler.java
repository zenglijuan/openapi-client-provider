package com.acooly.openapi.client.fudian.notify;

import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.api.notify.NotifyHandler;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 个人用户注册 异步通知处理器
 */
@Slf4j
@Component
public class UserRegisterNotifyHandler implements NotifyHandler {


    @Override
    public void handleNotify(ApiMessage notify) {
        log.info("{} 异步通知处理: {}", serviceKey(), notify);
    }

    @Override
    public String serviceKey() {
        return FudianServiceNameEnum.USER_REGISTER.code();
    }
}
