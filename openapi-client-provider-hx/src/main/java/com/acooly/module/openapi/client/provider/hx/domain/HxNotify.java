/**
 * create by zhangpu
 * date:2015年3月11日
 */
package com.acooly.module.openapi.client.provider.hx.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhangpu
 * 
 */
@Getter
@Setter
public class HxNotify extends HxResponse {

	/**
	 * 通知类型
	 */
	private String notify_type;

	/**
	 * 通知时间
	 */
	private String notify_time;
}
