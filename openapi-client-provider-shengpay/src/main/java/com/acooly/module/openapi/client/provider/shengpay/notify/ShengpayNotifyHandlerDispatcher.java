/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-26 10:53 创建
 */
package com.acooly.module.openapi.client.provider.shengpay.notify;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.ApiServiceClient;
import com.acooly.module.openapi.client.api.notify.AbstractSpringNotifyHandlerDispatcher;
import com.acooly.module.openapi.client.provider.shengpay.OpenAPIClientShengpayProperties;
import com.acooly.module.openapi.client.provider.shengpay.ShengpayApiServiceClient;
import com.acooly.module.openapi.client.provider.shengpay.ShengpayConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Fudian 专用异步通知分发器
 *
 * @author zhike 2017-09-26 10:53
 */
@Component
public class ShengpayNotifyHandlerDispatcher extends AbstractSpringNotifyHandlerDispatcher {

    @Autowired
    private ShengpayApiServiceClient shengpayApiServiceClient;

    @Autowired
    protected OpenAPIClientShengpayProperties openAPIClientYipayProperties;

    @Override
    protected String getServiceKey(String notifyUrl, Map<String, String> notifyData) {
        return getServiceName(notifyUrl);
    }

    @Override
    protected ApiServiceClient getApiServiceClient() {
        return this.shengpayApiServiceClient;
    }

    private String getServiceName(String notifyUrl) {
        String canonicalUrl = ShengpayConstants.getCanonicalUrl("/",
                openAPIClientYipayProperties.getNotifyUrl());
        String serviceName = Strings.substringAfter(notifyUrl, canonicalUrl);
        //匹配通知回调实体，不用前面多加/
//        if (!Strings.startsWith(serviceName, "/")) {
//            serviceName = "/" + serviceName;
//        }
        return serviceName;
    }
}
