/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.shengpay.domain;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zhike
 */
@Target({ ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface ShengpayAlias {

	/**
	 * 是否签名
	 * @return
	 */
	boolean sign() default true;

	/**
	 * 别名
	 * @return
	 */
	String value();

	/**
	 * 是否放入请求报文
	 * @return
	 */
	boolean request() default true;

	/**
	 * 是否加密
	 * @return
	 */
	boolean isEncrypt() default false;

	/**
	 * 是否解密
	 * @return
	 */
	boolean isDecode() default false;

}
