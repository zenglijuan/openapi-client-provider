package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscReturn;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscRemitTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscWithdrawFormEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscWithdrawStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscWithdrawTypeEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiMsgInfo(service = BoscServiceNameEnum.WITHDRAW, type = ApiMessageType.Return)
public class WithdrawReturn extends BoscReturn {
	/**
	 * 见【提现交易状态】
	 */
	@NotNull
	private BoscWithdrawStatusEnum withdrawStatus;
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 提现金额
	 */
	@MoneyConstraint(min = 0)
	private Money amount;
	/**
	 * 提现分佣
	 */
	@MoneyConstraint(nullable = true)
	private Money commission;
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 交易发起时间
	 */
	@NotNull
	private Date createTime;
	/**
	 * 交易完成时间
	 */
	@NotNull
	private Date transactionTime;
	
	/**
	 * 提现银行卡号
	 */
	@NotEmpty
	@Size(max = 50)
	private String bankcardNo;
	/**
	 * 出款类型；NORMAL：T1 出款；NORMAL_URGENT：普通 T0 出款；URGENT： 实时 T0 出款；
	 */
	@NotEmpty
	private BoscRemitTypeEnum remitType;
	/**
	 * 见【提现方式】
	 */
	@NotEmpty
	private BoscWithdrawTypeEnum withdrawWay;
	/**
	 * 提现类型：IMMEDIATE 为直接提现，CONFIRMED 为待确认提现
	 */
	@NotEmpty
	private BoscWithdrawFormEnum withdrawForm;
	/**
	 * 垫资金额
	 */
	@MoneyConstraint(nullable = true)
	private Money floatAmount;
	
	public BoscWithdrawStatusEnum getWithdrawStatus () {
		return withdrawStatus;
	}
	
	public void setWithdrawStatus (BoscWithdrawStatusEnum withdrawStatus) {
		this.withdrawStatus = withdrawStatus;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public Money getAmount () {
		return amount;
	}
	
	public void setAmount (Money amount) {
		this.amount = amount;
	}
	
	public Money getCommission () {
		return commission;
	}
	
	public void setCommission (Money commission) {
		this.commission = commission;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public Date getCreateTime () {
		return createTime;
	}
	
	public void setCreateTime (Date createTime) {
		this.createTime = createTime;
	}
	
	public Date getTransactionTime () {
		return transactionTime;
	}
	
	public void setTransactionTime (Date transactionTime) {
		this.transactionTime = transactionTime;
	}
	
	public String getBankcardNo () {
		return bankcardNo;
	}
	
	public void setBankcardNo (String bankcardNo) {
		this.bankcardNo = bankcardNo;
	}
	
	public BoscRemitTypeEnum getRemitType () {
		return remitType;
	}
	
	public void setRemitType (BoscRemitTypeEnum remitType) {
		this.remitType = remitType;
	}
	
	public BoscWithdrawTypeEnum getWithdrawWay () {
		return withdrawWay;
	}
	
	public void setWithdrawWay (BoscWithdrawTypeEnum withdrawWay) {
		this.withdrawWay = withdrawWay;
	}
	
	public BoscWithdrawFormEnum getWithdrawForm () {
		return withdrawForm;
	}
	
	public void setWithdrawForm (BoscWithdrawFormEnum withdrawForm) {
		this.withdrawForm = withdrawForm;
	}
	
	public Money getFloatAmount () {
		return floatAmount;
	}
	
	public void setFloatAmount (Money floatAmount) {
		this.floatAmount = floatAmount;
	}
}