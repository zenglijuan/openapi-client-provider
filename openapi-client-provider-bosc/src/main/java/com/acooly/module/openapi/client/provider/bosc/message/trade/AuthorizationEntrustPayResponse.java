package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscEntrustedAuthorizeStatusEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.AUTHORIZATION_ENTRUST_PAY, type = ApiMessageType.Response)
public class AuthorizationEntrustPayResponse extends BoscResponse {
	/**
	 * 委托支付授权审核状态，AUDIT(待审核),FAIL（授权失败）,PASSED(授权成功)
	 */
	@NotNull
	private BoscEntrustedAuthorizeStatusEnum authorizeStatus;
	
	public BoscEntrustedAuthorizeStatusEnum getAuthorizeStatus () {
		return authorizeStatus;
	}
	
	public void setAuthorizeStatus (
			BoscEntrustedAuthorizeStatusEnum authorizeStatus) {
		this.authorizeStatus = authorizeStatus;
	}
}