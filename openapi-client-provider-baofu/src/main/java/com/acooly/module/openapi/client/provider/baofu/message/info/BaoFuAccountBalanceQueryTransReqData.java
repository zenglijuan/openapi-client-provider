package com.acooly.module.openapi.client.provider.baofu.message.info;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/3/1 18:08
 */
@Getter
@Setter
@XStreamAlias("trans_reqData")
public class BaoFuAccountBalanceQueryTransReqData implements Serializable {

    /**
     * 账户类型
     * 0:全部；
     * 1:基本账户;
     * 2:未结算账户;
     * 3:冻结账户;
     * 4:保证金账户;
     * 5:资金托管账户；
     * 7:手续费账户
     * 9: 资金存管账户
     */
    @XStreamAlias("account_type")
    private String accountType;

    /**
     * 币种
     */
    @XStreamAlias("currency")
    private String currency;

    /**
     * 余额
     */
    @XStreamAlias("balance")
    private String balance;
}
