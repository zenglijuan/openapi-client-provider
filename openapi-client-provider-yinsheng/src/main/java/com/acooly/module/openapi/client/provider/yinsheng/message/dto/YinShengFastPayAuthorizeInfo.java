package com.acooly.module.openapi.client.provider.yinsheng.message.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

@Getter
@Setter
public class YinShengFastPayAuthorizeInfo implements Serializable {
    /**
     * 商户订单号
     */
    @NotBlank
    private String out_trade_no;
    /**
     * 手机号码
     */
    @NotBlank
    private String buyer_mobile;
    /**
     * 验证码
     */
    @NotBlank
    private String mobile_verify_code;


}
