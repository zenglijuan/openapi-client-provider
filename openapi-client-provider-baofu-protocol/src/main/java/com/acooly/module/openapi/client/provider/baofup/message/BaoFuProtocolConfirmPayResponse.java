package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPResponse;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/13 14:56
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.PROTOCOL_CONFIRM_PAY,type = ApiMessageType.Response)
public class BaoFuProtocolConfirmPayResponse extends BaoFuPResponse {

    /**
     * 成功金额
     * 单位：分
     * 例：1元则提交100
     */
    @Size(max = 12)
    @BaoFuPAlias(value = "succ_amt")
    private String succAmount;

    /**
     * 成功时间
     */
    @BaoFuPAlias(value = "succ_time")
    private String succTime;

    /**
     * 宝付订单号
     */
    @Size(max = 32)
    @BaoFuPAlias(value = "order_id")
    private String bankOrderNo;

    /**
     * 商户订单号
     * 唯一订单号，8-50 位字母和数字,未支付成功的订单号可重复提交
     */
    @NotBlank
    @Size(min = 8,max = 50)
    @BaoFuPAlias(value = "trans_id")
    private String transId;
}
