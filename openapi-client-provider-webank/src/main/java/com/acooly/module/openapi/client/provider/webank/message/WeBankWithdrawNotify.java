package com.acooly.module.openapi.client.provider.webank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankApiMsgInfo;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankNotify;
import com.acooly.module.openapi.client.provider.webank.enums.WeBankServiceEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@WeBankApiMsgInfo(service = WeBankServiceEnum.WEBANK_WITHDRAW, type = ApiMessageType.Notify)
public class WeBankWithdrawNotify extends WeBankNotify {

    /**
     * 商户号
     * 必填
     */
    private String merId;

    /**
     * 订单号
     * 必填
     */
    private String orderId;

    /**
     * 交易类型
     * 必填
     */
    private String bizType;

    /**
     * 请求日期
     * 必填
     */
    private String reqDate;

    /**
     * 请求时间
     * 必填
     */
    private String reqTime;

    /**
     * 交易金额
     * 以元为单位，最多两位小数
     */
    private String amount;

    /**
     * 交易状态
     * 必填
     */
    private String status;

    /**
     * 清算日期
     * 非必填
     */
    private String settleDate;

    /**
     * 响应码
     * 非必填
     */
    private String respCode;

    /**
     * 响应码描述
     * 非必填
     */
    private String respMsg;

    /**
     * 签名
     * 必填，不参与签名
     */
    private String signature;
}
