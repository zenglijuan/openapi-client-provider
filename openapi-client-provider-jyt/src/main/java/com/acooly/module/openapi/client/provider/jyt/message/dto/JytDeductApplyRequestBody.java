package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/5/8 0:05
 */
@Getter
@Setter
@XStreamAlias("body")
public class JytDeductApplyRequestBody implements Serializable {

    /**
     * 客户号
     * 持卡人在商户端客户号（一个身份证号对应一个客户号）
     */
    @Size(max = 30)
    @XStreamAlias("cust_no")
    @NotBlank
    private String userId;

    /**
     * 订单号
     * 商户订单号（全局唯一）
     */
    @Size(max = 32)
    @XStreamAlias("order_id")
    @NotBlank
    private String orderId;

    /**
     * 银行卡号
     */
    @Size(max = 30)
    @XStreamAlias("bank_card_no")
    @NotBlank
    private String bankCardNo;

    /**
     * 身份证号
     */
    @Size(max = 30)
    @XStreamAlias("id_card_no")
    @NotBlank
    private String idCardNo;

    /**
     * 手机号
     */
    @Size(max = 13)
    @XStreamAlias("mobile")
    @NotBlank
    private String mobileNo;

    /**
     * 姓名
     * 必须与持卡人在银行预留手机号一致
     */
    @Size(max = 30)
    @XStreamAlias("name")
    @NotBlank
    private String realName;

    /**
     * 金额
     */
    @Size(max = 15)
    @XStreamAlias("tran_amount")
    @NotBlank
    private String amount;


    /**
     * 银行编号
     * 银行卡对应的银行编码
     */
    @Size(max = 8)
    @XStreamAlias("bank_code")
    private String bankCode;

    /**
     * 信用卡有效期
     *信用卡的有效期，格式为MMYY（月月年年），如0116；
     *当为02信用卡时必输，
     *其他卡时，设值为null
     */
    @Size(max = 4)
    @XStreamAlias("expired_date")
    private String expiredDate;

    /**
     * 信用卡校验码
     与信用卡有效期共生，表示卡背面后3位数字。
     当为02信用卡时必输，
     其他卡时，设值为null
     */
    @Size(max = 3)
    @XStreamAlias("cvv2")
    private String cvv2;

    /**
     * 银行卡类型
     卡类型：01借记卡，02信用卡（需要特别说明）；默认借记卡
     */
    @Size(max = 2)
    @XStreamAlias("bank_card_type")
    private String bankCardType = "01";
}
