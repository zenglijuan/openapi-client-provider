package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/5/8 15:55
 */
@Getter
@Setter
@XStreamAlias("body")
public class JytDeductPayRequestBody implements Serializable {
    /**
     * 待支付订单号（全局唯一）
     */
    @Size(max = 32)
    @XStreamAlias("order_id")
    @NotBlank
    private String orderId;

    /**
     * 短信验证码
     * 持卡人收到的短信验证码
     */
    @Size(max = 10)
    @XStreamAlias("verify_code")
    @NotBlank
    private String smsCode;
}
