package com.acooly.module.openapi.client.provider.fbank.message;

import com.acooly.core.common.exception.OrderCheckException;
import com.acooly.core.common.facade.ResultCode;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankRequest;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @author zhike@qq.com
 * @date 2018-09-06 11:48
 */
@Getter
@Setter
@FbankApiMsgInfo(service = FbankServiceEnum.TRADE_QUERY, type = ApiMessageType.Request)
public class FbankTransQueryRequest extends FbankRequest {

    /**
     * 下单日期
     * 订单创建时间；格式：yyyyMMdd
     */
    @Size(max = 19)
    private String orderDt;


    /**
     * 商户订单号
     * 商户自己平台的订单号（唯一）
     * mchntOrderNo与 orderNo必须传一个
     */
    @Size(max = 64)
    private String mchntOrderNo;


    /**
     * 支付平台订单号
     * 支付平台生成的订单号（唯一）
     * mchntOrderNo与 orderNo必须传一个
     */
    @Size(max = 32)
    private String orderNo;

    @Override
    public String getService() {
        return FbankServiceEnum.TRADE_QUERY.getKey();
    }

    @Override
    public void doCheck() {
        super.doCheck();
        if (Strings.isBlank(mchntOrderNo) && Strings.isBlank(orderNo)) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(), "mchntOrderNo与 orderNo不能同时为空");
        }
    }
}
