package com.acooly.module.openapi.client.provider.fbank.message.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike@acooly.com
 * @date 2018-09-07 16:17
 */
@Getter
@Setter
@ToString
public class RefundOrderInfo implements Serializable {

    /**
     * 单笔退款号
     * 富民生成的退款订单号
     */
    @NotBlank
    @Size(max = 50)
    private String refOrderno;

    /**
     * 原订单号
     * 富民平台的原订单号
     */
    @Size(max = 20)
    private String orderNo;

    /**
     * 退款类型
     * 0全款，1部分
     */
    @Size(max = 1)
    private String refundType;

    /**
     * 退款金额
     * 退款金额（分）
     */
    @NotBlank
    @Size(max = 12)
    private String refundFee;

    /**
     * 退款创建时间
     * 退款成功时的时间（注意空格）
     * 格式：yyyyMMdd HHmmss
     */
    @NotBlank
    @Size(max = 64)
    private String refCreateDate;

    /**
     * 退款时间
     * 退款成功时的时间（注意空格）
     * 格式：yyyyMMdd HHmmss
     */
    @Size(max = 15)
    private String refDate;

    /**
     * 退款状态
     * （0待退款，1成功，2失败）
     */
    @NotBlank
    @Size(max = 2)
    private String status;

    /**
     * 退款理由
     * 退款的具体原因
     */
    @Size(max = 100)
    private String reason;

    /**
     * 商户退款订单号
     */
    @Size(max = 50)
    private String refMchntOrderNo;

}
