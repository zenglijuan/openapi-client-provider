/** create by zhike date:2015年3月11日 */
package com.acooly.module.openapi.client.provider.fbank.domain;

import com.acooly.module.openapi.client.provider.fbank.support.FbankAlias;
import lombok.Getter;
import lombok.Setter;

/** @author zhike */
@Getter
@Setter
public class FbankNotify extends FbankResponse {

  /** 返回状态码 */
  @FbankAlias(value = "status")
  private String status;

  /** 返回信息 */
  @FbankAlias(value = "message")
  private String message;

  /** --------------------------以下字段在 status 为 0的时候有返回-------------------------------- */

  /**
   * 业务结果 0表示成功，非0表示失败
   */
  @FbankAlias("result_code")
  private String resultCode;

  /**
   * 商户号
   */
  @FbankAlias("mch_id")
  private String mchId;

  /**
   * 设备号
   */
  @FbankAlias("device_info")
  private String deviceInfo;

  /**
   * 随机字符串
   */
  @FbankAlias("nonce_str")
  private String nonceStr;

  /**
   * 错误代码
   */
  @FbankAlias("err_code")
  private String errCode;

  /**
   * 错误代码描述
   */
  @FbankAlias("err_msg")
  private String errMsg;
}
