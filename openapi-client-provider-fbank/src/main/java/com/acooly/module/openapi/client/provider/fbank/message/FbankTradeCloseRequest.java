package com.acooly.module.openapi.client.provider.fbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankRequest;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @author zhike@acooly.com
 * @date 2018-09-07 17:38
 */
@Getter
@Setter
@FbankApiMsgInfo(service = FbankServiceEnum.TRADE_CLOSE, type = ApiMessageType.Request,isVerifySign = false)
public class FbankTradeCloseRequest extends FbankRequest {

    /**
     * 商户订单号
     * 商户自己平台的订单号（唯一）
     * mchntOrderNo与 orderNo必须传一个
     */
    @Size(max = 64)
    private String mchntOrderNo;

    /**
     * 支付平台订单号
     * 支付平台生成的订单号（唯一）
     * mchntOrderNo与 orderNo必须传一个
     */
    @Size(max = 32)
    private String orderNo;
}
