package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.acooly.module.openapi.client.provider.fuyou.support.FuyouAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:04
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_NETBANK_SYNCHRO_QUERY,type = ApiMessageType.Request)
public class FuyouNetBankSynchroQueryRequest extends FuyouRequest{

    /**
     * 商户代码
     * 富友分配给各合作商户的唯一识别码
     */
    @FuyouAlias("mchnt_cd")
    @NotBlank
    @Size(max = 15)
    private String mchntCd;

    /**
     * 商户订单号
     * 客户支付后商户网站产生的一个唯
     * 一的定单号，该订单号应该在相当
     * 长的时间内不重复。富友通过订单
     * 号来唯一确认一笔订单的重复性.
     */
    @FuyouAlias("order_id")
    @NotBlank
    @Size(max = 30)
    private String orderId;

    @Override
    public String getSignStr() {
        return getMchntCd()+"|"+getOrderId()+"|";
    }
}
