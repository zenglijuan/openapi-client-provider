/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 14:40 创建
 */
package com.acooly.module.openapi.client.provider.yl;


import com.acooly.module.openapi.client.provider.yl.enums.YlServiceEnum;
import com.acooly.module.openapi.client.provider.yl.message.YlBatchDeductRequest;
import com.acooly.module.openapi.client.provider.yl.message.YlBatchDeductResponse;
import com.acooly.module.openapi.client.provider.yl.message.YlDeductQueryRequest;
import com.acooly.module.openapi.client.provider.yl.message.YlDeductQueryResponse;
import com.acooly.module.openapi.client.provider.yl.message.YlRealDeductRequest;
import com.acooly.module.openapi.client.provider.yl.message.YlRealDeductResponse;
import com.acooly.module.openapi.client.provider.yl.message.YlbillDownloadRequest;
import com.acooly.module.openapi.client.provider.yl.message.YlbillDownloadResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author zhangpu 2017-09-25 14:40
 */
@Service
public class YlApiService {
	
	@Resource(name = "ylApiServiceClient")
	private YlApiServiceClient ylApiServiceClient;

	@Autowired
	private YlProperties ylProperties;
	
	/**
	 * 单笔代扣
	 * @param request
	 * @return
	 */
	public YlRealDeductResponse ylRealDeduct(YlRealDeductRequest request) {
		request.setServiceCode(YlServiceEnum.YL_REAL_DEDUCT.getCode());
		return (YlRealDeductResponse) ylApiServiceClient.execute(request);
	}

	/**
	 * 批量代扣
	 * @param request
	 * @return
	 */
	public YlBatchDeductResponse ylBatchDeduct(YlBatchDeductRequest request) {
		request.setServiceCode(YlServiceEnum.YL_BATCH_DEDUCT.getCode());
		return (YlBatchDeductResponse) ylApiServiceClient.execute(request);
	}

	/**
	 * 代扣查询
	 * @param request
	 * @return
	 */
	public YlDeductQueryResponse ylDeductQuery(YlDeductQueryRequest request) {
		request.setServiceCode(YlServiceEnum.YL_REAL_DEDUCT_QUERY.getCode());
		return (YlDeductQueryResponse) ylApiServiceClient.execute(request);
	}

	/**
	 * 交易订单查询
	 * @param request
	 * @return
	 */
	public YlbillDownloadResponse ylBillDownload(YlbillDownloadRequest request) {
		request.setServiceCode(YlServiceEnum.YL_BILL_DOWNLOAD.getCode());
		return (YlbillDownloadResponse)ylApiServiceClient.execute(request);
	}
}

