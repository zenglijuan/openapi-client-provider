package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftResponse;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2017/11/28 20:15
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.UNIFIED_TRADE_REFUND, type = ApiMessageType.Response)
public class WftUnifiedTradeRefundResponse extends WftResponse {

    /**
     * --------------------------以下字段在 status 和 result_code 都为 0的时候有返回--------------------------------
     */
    /**
     * 商户订单号
     */
    @WftAlias(value = "out_trade_no")
    private String outTradeNo;

    /**
     * 平台订单号
     */
    @WftAlias(value = "transaction_id")
    private String transactionId;

    /**
     * 商户退款单号
     */
    @WftAlias(value = "out_refund_no")
    private String outRefundNo;

    /**
     * 平台退款单号
     */
    @WftAlias(value = "refund_id")
    private String refundId;

    /**
     * 退款总金额,单位为分,可以做部分退款
     */
    @WftAlias(value = "refund_fee")
    private String refundFee;

    /**
     * ORIGINAL-原路退款，默认
     */
    @WftAlias(value = "refund_channel")
    private String refundChannel;

    /**
     * 现金券退款金额 <= 退款金额， 退款金额-现金券退款金额为现金
     */
    @WftAlias(value = "coupon_refund_fee")
    private String couponRefundFee;
}
