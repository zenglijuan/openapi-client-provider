package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.core.common.exception.OrderCheckException;
import com.acooly.core.common.facade.ResultCode;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftRequest;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2017/11/28 20:14
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.UNIFIED_TRADE_REFUND, type = ApiMessageType.Request)
public class WftUnifiedTradeRefundRequest extends WftRequest{

    /**
     * 商户订单号
     */
    @WftAlias(value = "out_trade_no")
    @Size(max = 32)
    private String outTradeNo;

    /**
     * 平台订单号
     */
    @WftAlias(value = "transaction_id")
    @Size(max = 32)
    private String transactionId;

    /**
     * 商户退款单号
     */
    @WftAlias(value = "out_refund_no")
    @Size(max = 32)
    @NotBlank(message = "退款订单号不能为空")
    private String outRefundNo;

    /**
     * 订单总金额，单位为分
     */
    @WftAlias(value = "total_fee")
    @NotBlank(message = "订单总金额不能为空")
    private String totalAmount;

    /**
     * 退款总金额,单位为分,可以做部分退款
     */
    @WftAlias(value = "refund_fee")
    @NotBlank(message = "退款总金额不能为空")
    private String refundAmount;

    /**
     * 操作员帐号,默认为商户号
     */
    @WftAlias(value = "op_user_id")
    @NotBlank(message = "操作员不能为空")
    @Size(max = 32)
    private String opUserId;

    /**
     * ORIGINAL-原路退款，默认
     */
    @WftAlias(value = "refund_channel")
    @Size(max = 16)
    private String refundChannel;
    /**
     * 订单号
     */
    @WftAlias(value = "nonce_str")
    @NotBlank(message = "随机字符串不能为空")
    @Size(max = 32)
    private String nonceStr;

    @Override
    public void doCheck() {
        if (Strings.isBlank(outTradeNo) && Strings.isBlank(transactionId)) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(), "商户订单号和业务订单号不能同时为空");
        }
    }
}
