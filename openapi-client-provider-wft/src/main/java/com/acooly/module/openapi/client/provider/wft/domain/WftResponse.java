/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.wft.domain;

import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * Fuiou 响应报文基类
 *
 * @author zhangpu
 */
@Getter
@Setter
public class WftResponse extends WftApiMessage {

  /** 返回状态码 */
  @WftAlias(value = "status")
  private String status;

  /** 返回信息 */
  @WftAlias(value = "message")
  private String message;

  /** --------------------------以下字段在 status 为 0的时候有返回-------------------------------- */

  /**
   * 业务结果 0表示成功，非0表示失败
   */
  @WftAlias("result_code")
  private String resultCode;

  /**
   * 商户号
   */
  @WftAlias("mch_id")
  private String mchId;

  /**
   * 设备号
   */
  @WftAlias("device_info")
  private String deviceInfo;

  /**
   * 随机字符串
   */
  @WftAlias("nonce_str")
  private String nonceStr;

  /**
   * 错误代码
   */
  @WftAlias("err_code")
  private String errCode;

  /**
   * 错误代码描述
   */
  @WftAlias("err_msg")
  private String errMsg;
}
