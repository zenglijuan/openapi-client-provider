package com.acooly.module.openapi.client.provider.newyl.message.xStream.deductQuery.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.List;

/**
 * @author fufeng 2018/1/26 15:29.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("QUERY_DETAILS")
public class QdReqQueryDetails {
    @XStreamImplicit(itemFieldName="QUERY_DETAIL")
    private List<QdReqQueryDetail> queryDetail;

}
